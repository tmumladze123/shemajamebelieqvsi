package com.example.retrofit

import com.example.retrofit.data.apartment
import retrofit2.Response
import retrofit2.http.GET

interface Api{
    @GET("")
    suspend fun getItem(): Response<apartment>
}