package com.example.retrofit.data

import com.google.gson.annotations.SerializedName

data class apartment(
    @SerializedName("content")
    val apartmentItem: List<apartmentItem>?
)