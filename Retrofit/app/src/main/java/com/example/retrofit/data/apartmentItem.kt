package com.example.retrofit.data

import com.google.gson.annotations.SerializedName

data class apartmentItem(
    val category: String,
    val cover: String,
    val created_at: Long,
    val descriptionEN: String,
    val descriptionKA: String,
    val descriptionRU: String,
    val id: String,
    val isLast: Boolean,
    @SerializedName("publish_date")
    val publishDate: String,
    val published: Int,
    val titleEN: String,
    val titleKA: String,
    val titleRU: String,
    val updated_at: Long
)