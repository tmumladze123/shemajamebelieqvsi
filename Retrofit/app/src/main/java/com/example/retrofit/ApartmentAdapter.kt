package com.example.retrofit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofit.data.apartment
import com.example.retrofit.data.apartmentItem
import com.example.retrofit.databinding.ItemBinding

class ApartmentAdapter : RecyclerView.Adapter<ApartmentAdapter.ViewHolder>(){
    /*var apartments: List<apartmentItem>
        get() =differ.currentList
        set(value) { differ.submitList(value) }*/
    var apartmentObj: MutableList<apartment>
        get() =differApart.currentList
        set(value) { differApart.submitList(value) }

    inner class ViewHolder(var binding: ItemBinding) : RecyclerView.ViewHolder(binding.root) {
    }
    private val diffCallback = object : DiffUtil.ItemCallback<apartmentItem>() {
        override fun areItemsTheSame(oldItem: apartmentItem, newItem: apartmentItem): Boolean {
            return oldItem.id == newItem.id
        }
        override fun areContentsTheSame(oldItem: apartmentItem, newItem: apartmentItem): Boolean {
            return oldItem == newItem
        }
    }
    private val diffCallbackApartment = object : DiffUtil.ItemCallback<apartment>() {
        override fun areItemsTheSame(oldItem: apartment, newItem: apartment): Boolean {
           return oldItem.apartmentItem==newItem.apartmentItem
        }
        override fun areContentsTheSame(oldItem: apartment, newItem: apartment): Boolean {
            return oldItem == newItem
        }
    }
    //private val differ = AsyncListDiffer(this, diffCallback)
    private val differApart = AsyncListDiffer(this, diffCallbackApartment)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int,
    ): ViewHolder =
        ViewHolder(ItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.apply {
            val item = apartmentObj.get(0).apartmentItem?.get(position)
            tvDescriptionKA.text=item?.descriptionKA
            tvTitleKA.text=item?.titleKA
            tvPublishDate.text=item?.publishDate
        }
    }

    override fun getItemCount(): Int =apartmentObj.size


}







